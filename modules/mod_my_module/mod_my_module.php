<?php
    /*
	Héctor Fabián Morales Ramírez
	Tecnólogo en Ingeniería de Sistemas
	Octubre 2013
*/

require_once(JPATH_ROOT."/myCore/autoload.php");

$controller = $params->get('controller');
$task = $params->get('task');
$_params = $params->get('params');
$args = [];
if (!empty($_params)){
    $partes = explode(",", $_params);
    foreach ($partes as $p){
        list($k, $v) = explode("=", $p);
        $args[$k] = $v;
    }
}
$c = myApp::getController($controller);
echo $c->ejecutar($task, $args);