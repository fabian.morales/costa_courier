<?php

class convenioController extends myController{
    public function index(){
        return $this->homeCiudad();
    }
    
    public function homeCiudad(){
        $tarea = myApp::getRequest()->getVar("task");
        $convenios = array();
        $ciudad = null;
        $idCiudad = myApp::getRequest()->getVar("id");
        $otrasCiudades = 0;
        if ($idCiudad == -1){
            $convenios = Convenio::whereHas("ciudades", function($query) {
                $query->where("id", ">", "3");
            })->where("estado", "A")->get();
            $otrasCiudades = 1;
        }
        else{
            $ciudad = CiudadConvenio::find($idCiudad);
            
            if (sizeof($ciudad)){
                $convenios = $ciudad->convenios()->where("estado", "A")->get();
            }
            else{
                $convenios = Convenio::where("estado", "A")->get();
            }
        }        
        return myView::render("convenio.index_ciudad", ["convenios" => $convenios, "ciudad" => $ciudad, "urlImg" => myApp::urlImg(), "tarea" => $tarea, "otrasCiudades" => $otrasCiudades]);
    }
    
    public function homeCategoria(){
        $tarea = myApp::getRequest()->getVar("task");
        $convenios = array();        
        $idCat = myApp::getRequest()->getVar("id");        
        $categoria = CategoriaConv::find($idCat);

        if (sizeof($categoria)){
            $convenios = $categoria->convenios()->where("estado", "A")->get();
        }
        else{
            $convenios = Convenio::where("estado", "A")->get();
        }
        
        $categorias = CategoriaConv::all();
        return myView::render("convenio.index_categoria", ["convenios" => $convenios, "categorias" => $categorias, "categoria" => $categoria, "ciudad" => array(), "urlImg" => myApp::urlImg(), "tarea" => $tarea]);
    }
    
    public function homeOfertas(){
        $tarea = myApp::getRequest()->getVar("task");
        $articulos = Articulo::where('catid', '8')->where('state', 1)->orderBy('created', 'desc')->get();        
        return myView::render("convenio.index_ofertas", ["articulos" => $articulos, "urlImg" => myApp::urlImg(), "tarea" => $tarea]);
    }
    
    public function homeBusqueda(){
        $categorias = CategoriaConv::all();
        $tarea = myApp::getRequest()->getVar("task");
        return myView::render("convenio.index_busqueda", ["categorias" => $categorias, "urlImg" => myApp::urlImg(), "tarea" => $tarea]);
    }
    
    public function detalleConvenio(){
        $id = myApp::getRequest()->getVar("id");
        $convenio = Convenio::where("id", $id)->with("ciudades")->first();
        return myView::render("convenio.detalle", ["convenio" => $convenio, "urlImg" => myApp::urlImg()]);
    }
    
    public function generarTopMenu(){
        $tarea = myApp::getRequest()->getVar("task");
        return myView::render("convenio.top_menu", ["tarea" => $tarea, "urlImg" => myApp::urlImg()]);
    }
}