create table occ_my_con_ciudad(
    id smallint not null primary key,
    nombre varchar(255) not null,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
);

create table occ_my_con_categoria(
    id smallint not null primary key,
    nombre varchar(255) not null,
    banner vharchar(255) not null,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' 
);

create table occ_my_con_convenio(
    id smallint not null primary key,
    id_categoria int,
    nombre varchar(255) not null,
    servicios text,
    descuento varchar(512),
    ubicacion varchar(255),
    telefono varchar(255),
    estado enum('A', 'I') default 'A',
    extension varchar(10),
    nombre_contacto varchar(255),
    email varchar(255),
    nit varchar(20),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_categoria) references occ_my_con_categoria (id)
);

create table occ_my_con_ciudadconv(
    id_ciudad integer,
    id_convenio integer,    
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_ciudad) references occ_my_con_ciudad (id),
    foreign key (id_convenio) references occ_my_con_convenio (id)
);
