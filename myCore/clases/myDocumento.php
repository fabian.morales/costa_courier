<?php
/*
	Héctor Fabián Morales Ramírez
	Tecnólogo en Ingeniería de Sistemas
	Enero 2011
*/
class myDocumento{
    public function myDocumento(){
        $document = JFactory::getDocument();
        $document->addStyleSheet(JUri::root()."myCore/css/my.css");
    }
    
    public function establecerTitulo($titulo){
        $document = JFactory::getDocument();
        $document->setTitle($titulo);
    }

    public function addScript($url){
        $document = JFactory::getDocument();
        $document->addScript($url);
    }

    public function addScripts($array_url){
        $document = JFactory::getDocument();
        foreach ($array_url as $url){
            $document->addScript($url);
        }
    }

    public function addEstilo($url){
        $document = JFactory::getDocument();
        $document->addStyleSheet($url);
    }

    public function addEstilos($array_url){
        $document = JFactory::getDocument();

        foreach ($array_url as $url){
            $document->addStyleSheet($url);
        }
    }

    public function incluirJQuery($version="1.10.2"){
        $document = JFactory::getDocument();
        $document->addScript(JURI::root().'myCore/js/jquery/jquery-'.$version.'.js');
        $document->addScriptDeclaration ('jQuery.noConflict();');
    }

    public function incluirLibJs($nombre, $css=null){
        $doc = JFactory::getDocument();
        $doc->addScript(JURI::root().'myCore/js/'.$nombre.'/'.$nombre.'.js');        
        if (sizeof($css)){
            foreach ($css as $c){
                $doc->addStyleSheet(JURI::root().'myCore/js/'.$nombre.'/'.$c.'.css');
            }
        }
    }
}