<?php

class Convenio extends myEloquent {    
    protected $table = 'my_con_convenio';
    protected $fillable = array('nombre', 'servicios', 'descuento', 'condiciones', 'ubicacion', 'telefono', 'id_categoria', 'estado', 'nombre_contacto', 'email');
    
    function categorias(){
        return $this->belongsTo('CategoriaConv', 'id_categoria');
    }
    
    function ciudades(){
        return $this->belongsToMany('CiudadConvenio', 'my_con_ciudadconv', 'id_convenio', 'id_ciudad');
    }
}
