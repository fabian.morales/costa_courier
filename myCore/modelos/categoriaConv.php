<?php

class CategoriaConv extends myEloquent {    
    protected $table = 'my_con_categoria';
    protected $fillable = array('nombre', 'descripcion');
    
    public function convenios(){
        return $this->hasMany('Convenio', 'id_categoria');
    }
}
