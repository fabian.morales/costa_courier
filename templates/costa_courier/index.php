<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
@ini_set('display_errors', '1');
// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
	$fullWidth = 1;
}
else
{
	$fullWidth = 0;
}

// Add Stylesheets
JHtml::_('jquery.framework', true, true);
//$doc->addScript(JURI::root(true).'/media/jui/js/jquery.min.js');
$doc->addStyleSheet('templates/'.$this->template.'/foundation/css/foundation.min.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/lightSlider/css/lightslider.css');
$doc->addStyleSheet('templates/'.$this->template.'/js/fancybox/fancybox.min.css');
$doc->addStyleSheet('templates/'.$this->template.'/css/style.css');

$doc->addScript('templates/'.$this->template.'/foundation/js/foundation.min.js');
$doc->addScript('templates/'.$this->template.'/foundation/js/vendor/modernizr.js');
$doc->addScript('templates/'.$this->template.'/js/script.js');
$doc->addScript('templates/'.$this->template.'/js/lightSlider/js/lightslider.min.js');
$doc->addScript('templates/'.$this->template.'/js/fancybox/fancybox.js');
// Add current user information
$user = JFactory::getUser();
$menu = $app->getMenu();
?>
<?php if(!$params->get('html5', 0)): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php else: ?>
	<?php echo '<!DOCTYPE html>'; ?>
<?php endif; ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <jdoc:include type="head" />
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>

	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
	<![endif]-->
</head>
<body>
    <div id="top-link"></div>   
    <div id="contenedor_top_menu">
        <div class="row">
            <div class="medium-3 large-4 columns small-only-text-center">
                <a href="<?php echo $this->baseurl ?>/index.php" id="lnkLogo">
                    <img src="templates/costa_courier/img/logo.png" alt="Costa Courier" />
                </a>
            </div>
            <div class="medium-9 large-8 columns">
                <div class="row">
                    <div class="small-12 columns">
                        <div class="right bloque redes">
                            <jdoc:include type="modules" name="top-redes" style="xhtml" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <div id="top-menu-resp">
                            <div>Menu</div>
                            <a href="#"></a>
                        </div>
                        <div id="top-menu-body" class="top_menu">
                            <jdoc:include type="modules" name="top-menu" style="xhtml" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <jdoc:include type="modules" name="banner" style="xhtml" />
       
    <div class="row text-center">
        <div class="small-12 columns">
            <jdoc:include type="modules" name="sub-banner" style="xhtml" />
        </div>
    </div>
    
    <?php if ($menu->getActive() != $menu->getDefault()) : ?>
        <div class="component">
            <div class="row">
                <div class="small-12 <?php if($this->countModules('lateral-derecho')) : ?> medium-8 <?php endif; ?> columns">
                    <jdoc:include type="component" />
                </div>
                <?php if($this->countModules('lateral-derecho')) : ?>
                <div class="small-12 medium-4 columns">
                    <jdoc:include type="modules" name="lateral-derecho" style="xhtml" />
                </div>
                <?php endif; ?>
            </div>
        </div>        
    <?php endif; ?>
    
    <div class="row">
        <div class="small-10 small-centered columns">
            <div class="row">
                <div class="small-12 columns">
                    <jdoc:include type="modules" name="sub-contenido" style="xhtml" />
                </div>
            </div>
        </div>
    </div>
    
    <div class="footer" id="footer">
        <div class="row">
            <div class="small-12 columns">
                <jdoc:include type="modules" name="footer" style="xhtml" />
            </div>
        </div>
        <div class="row copy">
            <div class="small-12 columns">
                &copy; 2015 Costa Courier, LLC
            </div>
        </div>
    </div>
</body>
</html>
