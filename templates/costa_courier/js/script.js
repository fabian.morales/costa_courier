(function(window, $){
    var slider_banner;
    var slider_servicios;

    var posMenu;
    var botones;

    function scrollTo(target) {
        var wheight = $(window).height();

        var fff = (wheight - $(target).height()) / 2
        var ooo = $(target).offset().top - fff;
        $('html, body').animate({scrollTop:ooo}, 600);
    }

    function ajusteTopMenu(){
        var scroll_top = $(window).scrollTop();

        /*if (scroll_top > posMenu) { 
            $('#contenedor_top_menu').css({ 'position': 'fixed', 'top':0, 'left':0, 'background-color' :'rgba(255,255,255,0.9)', 'z-index':1000 });
        } else {
            $('#contenedor_top_menu').css({ 'position': 'static' }); 
        }   */
    };

    function transicion(){
        //var anchoDisp = $('body').innerWidth();
        var anchoDisp = $(window).width();
        var anchoSeccion = anchoDisp - (botones.length * 60);
        var activo = $(".btnAcordeon.activo").attr("data-seccion");

        if (activo !== undefined && activo !== null){					
            $(activo).css("display", "block").css("width", anchoSeccion + "px");
        }

        botones.unbind("click");
        botones.bind("click", function () {
            activo = $(".btnAcordeon.activo").attr("data-seccion");

            if (!$(this).hasClass('activo')){
                var activoNuevo = $(this).attr("data-seccion");
                $(".btnAcordeon.activo").removeClass("activo");
                $(this).addClass("activo");

                $(activo).animate({ width: "0px" }, 500);
                $(activoNuevo).show().animate({ width: anchoSeccion + "px" }, 500);
            }
        });
    }
    
    function adicionarMarcador(mapa, lat, lng, info, img){
        var latlng = new google.maps.LatLng(lat, lng); 
        var marker = new google.maps.Marker({
                            position: latlng,
                            map: mapa,                                
                            html: info,
                            icon: img,
                            animation: google.maps.Animation.DROP, 
                            draggable: true
        });

        var infoWindow = new google.maps.InfoWindow({ content: marker.html });
        //infoWindow.open(mapa, marker);

        google.maps.event.addListener(marker, "click", function () {
            infoWindow.setContent(this.html);
            infoWindow.open(mapa, this);
        });
    }

    $(document).ready(function() {
        slider_servicios = $("#banner1").lightSlider({
            item: 1,
            pager: true,
            controls: true,
            auto: true
        });
        
        slider_servicios = $("#banner_gente").lightSlider({
            item: 1,
            pager: true,
            controls: true
        });
        
        slider_servicios = $("#banner_logos").lightSlider({
            item: 4,
            pager: true,
            controls: true
        });
        
        var latlng = new google.maps.LatLng(25.823513, -80.325703); 
        var options = { zoom: 13, center: latlng, mapTypeId: google.maps.MapTypeId.ROADMAP}; 
        var map = new google.maps.Map(document.getElementById("mapa"), options);
                
        adicionarMarcador(map, 25.823513, -80.325703, 'Costa Courier', undefined);
        
        $("#top-menu-resp > a").click(function(e){
            e.preventDefault();
            $("#top-menu-body").toggle("slow");
        });
                
        /*$(".top_menu a, a.mas_info").click(function(e) { 
            e.preventDefault();				
            scrollTo($(this).attr("data-target"));
        });
        
        $("#menu_servicios a").click(function(e) { 
            e.preventDefault();
            scrollTo("#slider_serv");
            slider_servicios.goToSlide($(this).attr("data-target"));
        });
        
        slider_servicios = $("#slider_serv").lightSlider({
            maxSlide: 1,
            pager: false,
            controls: true
        });*/
        /*$("#slider_serv").flexisel({
            visibleItems: 1,
            enableResponsiveBreakpoints: true,
            responsiveBreakpoints: {
                portrait: {
                    changePoint:480,
                    visibleItems: 1
                }, 
                landscape: {
                    changePoint:640,
                    visibleItems: 1
                },
                tablet: {
                    changePoint:768,
                    visibleItems: 1
                },
                grande: {
                    changePoint:1024,
                    visibleItems: 1
                }
            }
        });*/       

        botones = $(".btnAcordeon");
        transicion();

        //posMenu = $('#contenedor_top_menu').offset().top;
        //ajusteTopMenu();
        

        $(window).resize(function() { 
            transicion();
            ajusteTopMenu();
            if ($(window).width() > 700){
                $("#top-menu-body").show();
            }
            else{
                $("#top-menu-body").hide();
            }
        });

        $(window).scroll(function() {
            //ajusteTopMenu();
        });
        
        $("#btnTrackingMobile, #btnTrackingContent").click(function(e) {
            e.preventDefault();
            var $destino = $(this).attr("data-target");
            var $dato = $(this).attr("data-identifier");
            $.ajax({
                url: 'index.php?option=com_my_component&controller=instagram&task=obtenerTracking',
                data: { 'identifier' : $($dato).val(), 'format': 'raw' },
                success: function(res){
                    $($destino).html(res);
                }
            });
        });
        
        $("#btnTrackingAcordeon").click(function(e) {
            e.preventDefault();
            var $destino = $(this).attr("data-target");
            var $dato = $(this).attr("data-identifier");
            $.ajax({
                url: 'index.php?option=com_my_component&controller=instagram&task=obtenerTracking',
                data: { 'identifier' : $($dato).val(), 'format': 'raw' },
                success: function(res){
                    $($destino).html(res);
                    $.fancybox({ href: $destino });
                }
            });
        });
        
        //img_fullwidth
        
        /*if ($("#trackingForm").get(0)){
            Ext.onReady(function() {
                etr.app.setBusiness(3);
                new ecm.tracking.PanelEs ({
                    floating: false,
                    modal: false,
                    closable: false,
                    title: null,
                    height: 250,
                    width: 590,
                    defaults: {
                        labelAlign: 'top',
                        margin: '10 0 19 0',
                        allowBlank: false
                    },
                    renderTo: "trackingForm"
                });
            });
        }
        
        if ($("#trackingFormKMobile").get(0)){
            Ext.onReady(function() {
                etr.app.setBusiness(3);
                new ecm.tracking.PanelEs ({
                    floating: false,
                    modal: false,
                    closable: false,
                    title: null,
                    height: 300,
                    width: 300,
                    defaults: {
                        labelAlign: 'top',
                        margin: '10 auto 19 auto',
                        allowBlank: false
                    },
                    renderTo: "trackingFormMobile"
                });
            });
        }*/
    });

})(window, jQuery);